#!/bin/bash

# Script to run Pentaho in a Docker container, as a daemon with config in place to run the DataExtractor job found in
#   'image/repository/DataExtractor'.
# See global variables below for the default container name and path to tmp space.
# It will:-
#   1) mount config file 'config/source-table-definitions.csv' into the correct repository path to help run the DataExtractor job.
#   2) supply environment variables found in 'config/env-file.txt' into the container so they can drive the DataExtractor job (database connections etc).
#   3) mount host tmp space into the path matched by variable $WORKING_DATA_DIR set in kettle.properties, which is used by the DataExtractor job as its own tmp space.
# See also file 'image/kettle-home/.kettle/kettle.properties' for other environment variables available to the DataExtractor job.

container_name=pentaho-demo
working_data_dir=/tmp/pentaho-data
script_dir=$(dirname "$0")
error_message="Error finding config files!"

# Create host tmp directory.

mkdir -p "${working_data_dir}"

# Find the absolute path to the config files.

if cd "${script_dir}/config"; then  # if we can cd into the config dir...
  config_dir=$(pwd)
  cd "$OLDPWD" || { echo "${error_message}"; exit 1; }
else  # else if we failed to cd into the config dir...
  echo "${error_message}!"
  exit 1
fi

# Start the Docker container in the background.
# Mount host tmp directory into Pentaho.
# Mount local config file into the PDI repo so we can test the DataExtractor.
# Set env variables including AWS credentials (obfuscated with encr.sh script).
# As an alternative consider scripting with AWS CLI instead, e.g.:
#   $(aws configure get aws_access_key_id --profile test123)
#   $(aws configure get aws_secret_access_key --profile test123)

docker run -d --rm \
  --name "${container_name}" \
  -p 8443:8443 \
  --mount type=bind,source=/tmp/pentaho-data,target=/root/pentaho-data \
  --mount type=bind,source="${config_dir}/source-table-definitions.csv",target=/opt/pentaho/repository/DataExtractor/config/source-table-definitions.csv \
  --env-file "${config_dir}/env-file.txt" pdi-code-ssl

# SCRATCH FOR FUTURE USE:
# Optionally mount a host path git repo into the container (not one of its subfolders) so Pentaho jobs can do a `git pull` regularly and be dynamically updated.
#  --mount type=bind,source=$HOME/github.com/your-organisation/pentaho-demo,target=/root/git-repo-mounted-from-docker-host-filesystem \
# Optionally mount git-credentials file into the container to support job "Tools/Repo Refresher/GIT PULL.kjb"
#  --mount type=bind,source=$HOME/git-credentials,target=/root/git-credentials \
