#!/bin/bash
# Script to build the Docker image - nothing too complicated.

script_path=$(dirname "$0")

docker build -t pdi-code-ssl "$script_path/image"
