-----------------------------------------------------------------------------------------------------------------------
-- Create a procedure to test performance of INSERT/UPDATE triggers on our source table SSDE_DEMO_A.
-- Loop 10 times through:
--   update a column for the entire table and record start and end timing.
-- Dump the average duration per execution to DBMS_OUTPUT.
-----------------------------------------------------------------------------------------------------------------------

create or replace procedure pr_perf_test_iu_ssde_demo_a as 
    l_start timestamp;
    l_end timestamp;
    tmp_start timestamp;
    tmp_end timestamp;
    procedure pr_test(l_int in pls_integer) is 
    begin
        dbms_output.put('test '||l_int||': ');
        tmp_start := systimestamp;
        update ssde_demo_a set owner = 'RICHARD TEST '||l_int;
        commit;
        tmp_end := systimestamp;
        dbms_output.put_line(tmp_end - tmp_start);
    end;
begin    
    l_start := systimestamp;
    for x in 1..10 loop
        pr_test(x);
    end loop;    
    l_end := systimestamp;
    dbms_output.put_line('average: ' || (l_end - l_start)/10);
end;
/

-----------------------------------------------------------------------------------------------------------------------
-- Execute the performance test procedure to gather timing of INSERT/UPDATE triggers disabled vs enabled...
-----------------------------------------------------------------------------------------------------------------------

begin 
    dbms_output.put_line('performance test the INSERT/UPDATE trigger...');
    dbms_output.put_line('disable triggers');
    execute immediate 'alter table ssde_demo_a disable all triggers';
    pr_perf_test_iu_ssde_demo_a();
    
    dbms_output.put_line('enabled triggers');
    execute immediate 'alter table ssde_demo_a enable all triggers';
    pr_perf_test_iu_ssde_demo_a();
end;
/

-----------------------------------------------------------------------------------------------------------------------
-- Create a procedure to test performance of the DELETE trigger on our source table SSDE_DEMO_A.
-- Loop 10 times through:
--   delete all rows
--   capture timing
--   rollback
-- Dump the average duration per execution excluding the rollbacks.
-- The first execution will grow the audit table for the first time so it is expected to be slower.
-----------------------------------------------------------------------------------------------------------------------

create or replace procedure pr_perf_test_d_ssde_demo_a as 
    l_count pls_integer;
    l_start timestamp;
    l_end timestamp;
    tmp_start timestamp;
    tmp_end timestamp;
    l_duration_disabled interval day to second;
    l_duration_enabled interval day to second;
    procedure pr_test(l_int in pls_integer) is 
    begin
        dbms_output.put('test '||l_int||': ');
        tmp_start := systimestamp;
        delete ssde_demo_a;
        tmp_end := systimestamp;
        dbms_output.put_line(tmp_end - tmp_start);
        rollback;
    end;
begin    
    dbms_output.put_line('performance test the DELETE trigger...');
    select count(*) into l_count from ssde_demo_a;
    dbms_output.put_line('count ssde_demo_a: '||l_count);
    -- DISABLED
    dbms_output.put_line('disable triggers');
    execute immediate 'alter table ssde_demo_a disable all triggers';
    l_start := systimestamp;
    for x in 1..10 loop
        pr_test(x);
    end loop;    
    l_end := systimestamp;
    l_duration_disabled := (l_end - l_start)/10;
    dbms_output.put_line('average: ' || l_duration_disabled);
    -- ENABLED    
    dbms_output.put_line('enabled triggers');
    execute immediate 'alter table ssde_demo_a enable all triggers';
    l_start := systimestamp;
    for x in 1..10 loop
        pr_test(x);
    end loop;    
    l_end := systimestamp;
    l_duration_enabled := (l_end - l_start)/10;
    dbms_output.put_line('average: ' || l_duration_enabled);
    -- Difference
    dbms_output.put_line('difference: '        || (l_duration_enabled - l_duration_disabled));
    dbms_output.put_line('difference per row: '|| (l_duration_enabled - l_duration_disabled) / l_count);
end;
/

-----------------------------------------------------------------------------------------------------------------------
-- Execute the performance test procedure to gather timing of DELETE triggers disabled vs enabled...
-----------------------------------------------------------------------------------------------------------------------

begin 
    pr_perf_test_d_ssde_demo_a();
end;
/

-----------------------------------------------------------------------------------------------------------------------
-- SCRATCH
-----------------------------------------------------------------------------------------------------------------------

exit;

desc ssde_deleted_records;
insert into ssde_deleted_records (table_name, table_key) values ('ssde_demo_a','1');

select count(*) from ssde_demo_a;
select count(*) from ssde_deleted_records;

select * from ssde_deleted_records;


-- populate SSDE_DEMO_A with sample data...
begin
    execute immediate 'truncate table ssde_demo_a';
    for x in 1..10 loop
        insert into ssde_demo_a select * from ssde_demo_b where rownum <= 1000000;
        commit;
    end loop;
end;
/

delete ssde_demo_a;
truncate table ssde_deleted_records reuse storage;
