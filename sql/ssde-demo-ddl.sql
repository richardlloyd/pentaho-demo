-----------------------------------------------------------------------------------------------------------------------
-- Create sample data for Super Simple Extractor demo.
-----------------------------------------------------------------------------------------------------------------------

drop table ssde_demo_a purge;
drop table ssde_demo_b purge;

-- create demo table source...
create table ssde_demo_a as select * from all_objects where 1=0;
alter table ssde_demo_a add (last_modified_date date);

-- create demo table target...
create table ssde_demo_b as select * from ssde_demo_a where 1=0;

-- add sample data to the target...
insert into ssde_demo_b (
    select * from (
        -- grab some data from ALL_OBJECTS...
        (select * from all_objects where owner != 'RICHARD' and rownum <= 10000)
        cross join 
        -- generate a range of dates...
        (select sysdate - 10 + level last_modified_date
            from dual
            connect by level <= 10
        )
    )
);

-- add sample data to the source...
insert into ssde_demo_a 
    select * 
    from ssde_demo_b 
    where rownum <= 10000;

commit;

-----------------------------------------------------------------------------------------------------------------------
-- Track changes to the source data SSDE_DEMO_A
-- with a trigger that sets LAST_MODIFIED_DATE on INSERT or UPDATE, for each row.
-----------------------------------------------------------------------------------------------------------------------

create or replace trigger trg_ssde_demo_a_last_modified
    before update or insert on  ssde_demo_a for each row
    declare
    begin
      :new.last_modified_date := sysdate;
    end;
/

-----------------------------------------------------------------------------------------------------------------------
-- Create a table to capture DELETED records.
-----------------------------------------------------------------------------------------------------------------------

drop table ssde_deleted_records purge;
drop sequence seq_ssde_deleted_records;

create sequence seq_ssde_deleted_records start with 1 increment by 1 cache 50;

create table ssde_deleted_records (
    seq number primary key,
    table_name varchar2(30),
    table_key varchar2(4000),
    deleted_at_date date,
    purgable_y varchar2(1)
    );

create index idx_ssde_deleted_records_1 on ssde_deleted_records (table_name, table_key);       

create or replace trigger trg_ssde_deleted_records_seq
    before insert on ssde_deleted_records for each row
    declare
    begin
        :new.seq := seq_ssde_deleted_records.nextval;   
        :new.deleted_at_date := sysdate;
    end;
/

-----------------------------------------------------------------------------------------------------------------------
-- Track DELETED records in source table SSDE_DEMO_A using a trigger.
-- Adds the deleted PK to table SSDE_DELETED_RECORDS, for each row.
-- For the sake of this test, just choose a single column as the PK for now.
-----------------------------------------------------------------------------------------------------------------------

create or replace trigger trg_ssde_demo_a_deletes
    before delete on ssde_demo_a for each row
    declare
    begin
      insert into ssde_deleted_records (table_name, table_key) values ('SSDE_DEMO_A', :old.object_id);
    end;
/

-----------------------------------------------------------------------------------------------------------------------
-- SCRATCH
-----------------------------------------------------------------------------------------------------------------------

exit;

select count(*) from ssde_demo_a;
select count(*) from ssde_demo_b;

--select * from ssde_demo_a;
--select ','||column_name from user_tab_columns where table_name = 'SSDE_DEMO_A' order by column_id;
--delete ssde_demo_a where rownum <= 100000;
--truncate table ssde_demo_a;
--truncate table ssde_demo_b;
