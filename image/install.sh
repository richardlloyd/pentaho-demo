#!/bin/bash
# Script to automate installation of Pentaho environment specific files on users' Macs.
# For usage, execute the script without args.

script_path=`dirname $0`
install_source="${script_path}/kettle-home/.kettle"
install_target="${HOME}"  # optionally override this with -t switch below.
source_dir=`basename ${install_source}`
repo=`echo "$(cd "$(dirname "${script_path}/repository")"; pwd)/$(basename "${script_path}/repository")"`  # logical file repo configured in repositories.xml. optionally override with -r switch below.
repo_escaped=$(echo "${repo}" | sed 's/\//\\\//g')  # used by sed search and replace below.
debug=0 
install=0

function log() {
  if [[ "$debug" -eq 1 ]]; then 
    echo "$1"
  fi
}

function getargs {
  while getopts 'idr:t:' flag; do
    case "${flag}" in
      i) install=1 ;;
      r) repo=`echo "$(cd "$(dirname "${OPTARG}")"; pwd)/$(basename "${OPTARG}")"` ;;
      d) debug=1 ;;
      t) install_target="${OPTARG}" ;;
      *) echo "Unexpected option ${flag}" >&2; exit 1;;
    esac
  done
}

function dumpvariables {
  log "install=$install"
  log "repo=$repo" 
  log "repo_escaped=$repo_escaped"
  log "debug=$debug"
  log "install_source=$install_source"
  log "source_dir=$source_dir"
  log "install_target=$install_target"
  log "script_path=$script_path"
}

function usage {
  if [[ "$install" -ne 1 ]]; then
    echo ""
    echo "  Script to set up your Pentaho environment as follows:"
    echo ""
    echo -e "\t1) Directory '${install_source}' will be copied to target"
    echo -e "\t   installation directory '${install_target}/${source_dir}'"
    echo -e "\t2) The logical file repository configured in repositories.xml will be set to"
    echo -e "\t   '${repo}'"
    echo -e "\t3) The file path set in kettle.properties variable REPO_BASE_PATH_HOSTFILESYSTEM"
    echo -e "\t   will be set to match the repository path above."
    echo ""
    echo "  Usage:"
    echo "" 
    echo -e "\t-i    Execute the installation"
    echo -e "\t[-r]  Supply an alternative path (relative or absolute) to override the logical"
    echo -e "\t      file respository configured in repositories.xml"
    echo -e "\t[-t]  Supply an alternative path to override the target installation directory"
    echo -e "\t      If you use this then set KETTLE_HOME to match before running Pentaho"
    echo -e "\t[-d]  Enable debug logging"
    echo ""
    exit 1
  fi
}

function checktargetdir {
  if [[ -d "${install_target}/${source_dir}" ]]; then  # if target install dir exists then abort...
    echo "ERROR - Directory '${install_target}/${source_dir}' already exists. Remove this manually to continue." >&2
    exit 1
  fi
}

function checkrepodir {
  if [[ ! -d "${repo}" ]]; then  # if the repo directory does not exists then abort...
    echo -e "ERROR - Pentaho repository directory '${repo}' does not exist." >&2
    echo -e "ERROR - Specify a valid path to continue." >&2
    exit 1
  fi
}

function install {
  checktargetdir  # abort if target directory exists.
  checkrepodir  # abort if repo directory does NOT exist.
  # Create target installation path if it doesn't exist.
  if [[ ! -d "${install_target}" ]]; then
    echo "Creating directory '${install_target}'"
    mkdir -p "${install_target}"
  fi
  # Start the installation.
  echo "Copying '${install_source}' to '${install_target}/${source_dir}'..."
  # Copy the files.
  cp -rp "${install_source}" "${install_target}"
  # Fix the repository directory in repositories.xml.
  echo "Fixing the logical file repository path..."
  sed -i '' "s/<base_directory>.*<\/base_directory>/<base_directory>${repo_escaped}<\/base_directory>/" "${install_target}/${source_dir}/repositories.xml"
  # Fix the repository path in kettle.properties - optionally used by PDI code.
  sed -i '' "s/REPO_BASE_PATH_HOSTFILESYSTEM=.*/REPO_BASE_PATH_HOSTFILESYSTEM=${repo_escaped}/" "${install_target}/${source_dir}/kettle.properties"
  echo "Yippeeeee - installation complete."
}

getargs "$@"
dumpvariables
usage
install

exit 0