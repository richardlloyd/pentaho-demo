#!/bin/bash
# Script to start Carte (jetty) server, optionally using SSL.
# Supply "ssl" as parameter 1 to start jetty using SSL/TLS.

usessl="${1}"
script="${SCRIPT_HOME}/add-env-variables-to-kettle-properties.sh"

# Add current environment variables to kettle.properties.
$script

if [[ "$?" -ne 0 ]]; then
  echo "ERROR - unable to add environment variables to kettle.properties.\nDo you need to supply AWS or database connection variables to Docker?\nSee also script '${script}'."
fi

# Start carte as s standalone slave.
if [[ "${usessl}" == "ssl" ]]; then
  "${PDI_HOME}/carte.sh" "${CARTE_CONFIG}/carte-standalone-slave-with-ssl.xml"
else
  "${PDI_HOME}/carte.sh" "${CARTE_CONFIG}/carte-standalone-slave.xml"
fi
