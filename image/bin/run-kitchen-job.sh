#!/bin/bash

script="${SCRIPT_HOME}/add-env-variables-to-kettle-properties.sh"

if [[ $# -lt 1 ]]; then
  echo "Error - please supply a job name to run as \$1"
  exit 1
fi

# Add current AWS environment variables to kettle.properties.
$script

if [[ "$?" -ne 0 ]]; then
  echo "ERROR - unable to add environment variables to kettle.properties.\nDo you need to supply AWS or database connection variables to Docker?\nSee also script '${script}'."
fi

echo "Running job '${1}'..."

"${PDI_HOME}/kitchen.sh" -rep File-Repository -job "${1}"
