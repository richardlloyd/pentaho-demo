#!/bin/bash
# Script to populate kettle.properties file with all environment variables.

DEBUG=shift;  # supply -d to enable debug output.
KETTLE_PROPS_FILE="${KETTLE_HOME}/.kettle/kettle.properties"

function log {
  if [[ "${DEBUG}" == "-d" ]]; then
    echo "DEBUG: ${1}"
  fi
}

# Populate kettle properties with all environment variables.
env | sort | while read lin; do
  echo "${lin}" >>"${KETTLE_PROPS_FILE}"
  log "Adding to ${KETTLE_PROPS_FILE}: ${lin}"
done
