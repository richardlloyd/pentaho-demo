#!/bin/bash

# Script to generate a new SSL cert with basic password. 
# Contents used in this script must remain in sync with values found in file $CARTE_CONFIG_FILE.
# TODO: improve password randomness and output encrypted version to build pipeline.

# To workaround Chrome warnings:
#   See StackOverflow link below for notes on how to get Chrome to accept self-signed localhost certs:
#   https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate/42917227#42917227
#   One of thesuggestions was to paste this URL into your browser: 'chrome://flags/#allow-insecure-localhost' 
#   and enable the dropdown that will allow invalid certificate resources for localhost.
#   Valid for Chrome 83 running on MacOS 10.15.5 on 2020-06-05.

# Global variables.

CARTE_CONFIG_FILE="/opt/pentaho/carte-config/carte-standalone-slave-with-ssl.xml"
KEYSTORE_FILE="/opt/pentaho/carte-config/keystore"

# Remove any old keystore.

rm -f "${KEYSTORE_FILE}" 

# Generate a new keystore with basic password.
# Ensure SubjectAlternateName (SAN) contains 'localhost'.
# Ensure keystore passwords used below are aligned with the Carte slave config found in $CARTE_CONFIG_FILE.

keytool \
  -keystore "${KEYSTORE_FILE}" \
  -storetype pkcs12 \
  -alias jetty \
  -genkey \
  -noprompt \
  -dname 'CN=carteserver,OU=ReesLloyd,O=ReesLloyd' \
  -storepass cluster123 \
  -keypass cluster123 \
  -keyalg RSA \
  -sigalg SHA256withRSA \
  -validity 1800 \
  -ext san=dns:localhost

# Export the server cert...

keytool -keystore "${KEYSTORE_FILE}" -exportcert -storepass cluster123 -alias jetty -file ~/jetty.crt

# Trust the exported cert by importing it into the main Java cacerts keystore...
# This allows Pentaho jobs to dynamically launch other jobs onto this carte server.

keytool -keystore "${JAVA_HOME}/lib/security/cacerts" -importcert -file ~/jetty.crt -alias jetty -storepass changeit -noprompt

# Set up carte config XML file to point to the new keystore generated above.

ESCAPED_KEYSTORE_FILE=$(echo "${KEYSTORE_FILE}" | sed 's/\//\\\//g')
sed -i "s/__REPLACE_ME_WITH_FULL_PATH_TO_JAVA_KEYSTORE__/${ESCAPED_KEYSTORE_FILE}/" "${CARTE_CONFIG_FILE}"
