## tl;dr

For context, follow links in the detailed set-up instructions section below. 

If you're happy to wing it, try these steps:

1. Edit this [CSV file](../../../config/source-table-definitions.csv) to define the tables/views to extract 
1. Edit these [environment variables](../../../config/env-file.txt) to define your Oracle connections and AWS access keys
1. Build the Docker image: [docker-build.sh](../../../docker-build.sh)
1. Start Carte using: [docker-run.sh](../../../docker-run.sh)
1. Wait for Carte to start: `docker logs -f pentaho-demo`
1. Launch a "DataExtractor" job:

   ```bash
   # Set the value of MY_GROUP_ID in the URL below to match the group found in your CSV file entries edited above.
   curl -k -u cluster:cluster https://localhost:8443/kettle/executeJob/\?rep\=File-Repository\&job\=DataExtractor/EXTRACT%20TABLES%20IN%20GROUP\&level\=Minimal\&MY_GROUP_ID\=1
   ```

## Pentaho Code In This Directory

Start by reading jobs named in upper-case.


## Detailed Set-up Instructions

Please see the following links:

* [LinkedIn Article: "Migrating Oracle To Snowflake Using Pentaho - The Pipeline"](https://www.linkedin.com/pulse/migrating-oracle-snowflake-using-pentaho-pipeline-richard-lloyd/)
* [Google Slides: "Super Simple Data Extractor"](https://docs.google.com/presentation/d/11OUPDJX_RN47nmBe03irmphnjzZdYfzIZiqebnXeZj0/)


## Docker Image

See also [this DockerHub repo](https://hub.docker.com/repository/docker/relloyd/pdi-ce-8.1-awscli) and this [Dockerfile](../../Dockerfile) 
to understand how Pentaho has been containerised.
